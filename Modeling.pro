#-------------------------------------------------
#
# Project created by QtCreator 2010-07-15T20:41:10
#
#-------------------------------------------------

QT       += core gui

TARGET = Modeling
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
